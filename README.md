Simple Confluence add-on that renders HTML content
==================================================

Macro body contents are rendered on the page as HTML.  One parameter is provided that allows CSS web resources
to be included in the rendered page when set.

* Macro key: `html-renderer`
* Macro name: `HTML Renderer`