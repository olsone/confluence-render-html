'use strict';

// cheap window parm parsing, all parms dumped as strings into the parms map:
var parms = {};
$.each($(window.location.search.substring(1).split('&')), function(i, pair) {
    if (pair) {
        pair = pair.split('=');
        parms[pair[0]] = window.decodeURIComponent(pair[1].replace(/\+/g, '%20'));
    }
});

function loadJs(scripts) {
    if (scripts && scripts.length) {
        $.getScript(scripts.shift(), function() {
            loadJs(scripts);
        });
    }
}

/**
 * Stripped down function that takes storage format and converts to view format via REST call.  A side-effect
 * is that CSS web resources are loaded into the page before the promise is resolved.  NOTE, no javascript
 * web resources are loaded in this version.
 */ 
var convertToViewFormat = function(contextId, storageFormat) {
    var deferred = $.Deferred();
    
    AP.request({
        url: '/rest/api/contentbody/convert/view?expand=webresource.superbatch.uris.css,webresource.superbatch.uris.js,webresource.tags.all,webresource.uris.css,webresource.uris.js',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            value: storageFormat,
            representation: 'storage',
            content: {
                id: contextId
            }
        }),
        success: function(resp) {
            var r = JSON.parse(resp);
            var css = r.webresource.superbatch.uris.css || [];
            css = css.concat(r.webresource.uris.css || []);

            var js = r.webresource.superbatch.uris.js || [];
            js = js.concat(r.webresource.uris.js || []);
            
            // waiting to render until after all CSS has been loaded to avoid unstyled content.
            var cssLoaded = 0;
            $(css).each(function(i, cssSrc) {
                $('<link>').appendTo('head').attr({
                    type: 'text/css', 
                    rel: 'stylesheet',
                    media: 'all',

                    // make sure relative CSS ref's are absolute to the instance:
                    href: cssSrc.startsWith('//') ? cssSrc : parms.xdm_e + cssSrc
                }).load(function() {
                    cssLoaded++;
                    
                    // if we think all css has been loaded, go ahead and resolve with the content
                    if (cssLoaded === css.length) {
                        // replace all relative src attributes with fully-resolved version:
                        deferred.resolve($(r.value.replace(/src="\//g, 'src="' + parms.xdm_e + '/')));
                        loadJs(js);
                    }
                });
            });
        },
        error: deferred.reject
    });
    
    return deferred.promise();
}

$.getScript(parms.xdm_e + parms.cp + '/atlassian-connect/all.js', function() {
    AP.request({
        url: AJS.template('/rest/api/content/{pageId}/history/{pageVersion}/macro/id/{macroId}').fill(parms).toString(), 
        success: function(resp) {
            var content = JSON.parse(resp).body;
            if (parms.convert === 'true') {
                // NOTE we are not passing anything in to convert, and also not doing anything with the converted value:
                convertToViewFormat(parms.pageId, '').done(function() {
                    $(document.body).append(content);
                });
            } else {
                $(document.body).html(content);
            }
        }
    });
});